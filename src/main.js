import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Vuex from 'vuex'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


Vue.config.productionTip = false;

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    showGameNews: true,
    showGameNavigator: false,
    showGameHotelview: true,
    showGameRoom: false,
    showGameCreateroom: false
  },
  mutations: {
    updateGameNews(state){
      if(state.showGameNews === false){
        state.showGameNews = true;
      }else{
        state.showGameNews = false;
      }
    },
    updateGameNavigator(state){
      if(state.showGameNavigator === false){
        state.showGameNavigator = true;
      }else{
        state.showGameNavigator = false;
      }
    },
    updateGameCreateroom(state){
      if(state.showGameCreateroom === false){
        state.showGameNavigator = false;
        state.showGameCreateroom = true;
      }else{
        state.showGameCreateroom = false;
      }
    },
    showGameHotelview(state){
      if(state.showGameRoom === true){
        state.showGameRoom = false;
      }

      if(state.showGameHotelview === false){
        state.showGameHotelview = true;
      }
    },
    showGameRoom(state){
      if(state.showGameHotelview === true){
        state.showGameHotelview = false;
      }

      if(state.showGameRoom === false){
        state.showGameRoom = true;
      }
    }
  }
})

new Vue({
  router,
  store: store,
  render: h => h(App)
}).$mount('#app')
